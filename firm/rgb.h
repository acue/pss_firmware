/*
 *
 * Affective Centered User Experiences
 * Portable Sensoring System (PSS) firmware
 * Copyright 2014 David Pello & Luis Diaz
 * fabLAB Asturias
 * LABoral Centro de Arte y Creacion Industrial
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this code.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __RGB_H
#define __RGB_H

#include <inttypes.h>

// DEFINES
#define     LED_R    5
#define     LED_G    6
#define     LED_B    9

// RGB LED
typedef struct RGB
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
}RGB_t;

// color index
enum
{
  BLACK,
  RED,
  GREEN,
  BLUE,
  YELLOW,
  CYAN,
  PINK,
  WHITE,
};

typedef struct RGB_Color
{
  uint8_t rgb[3];
} RGB_Color_t;



void put_color(RGB_t* l, uint8_t r, uint8_t g, uint8_t b);  // update color values
void put_color_value(RGB_t* l, uint8_t col);      // update color values from color table
void set_RGB(RGB_t* l); // Set LED color
void set_RGB_value(uint8_t col);  // Set RGB color from color table

#endif
