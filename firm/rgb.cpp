/*
 *
 * Affective Centered User Experiences
 * Portable Sensoring System (PSS) firmware
 * Copyright 2014 David Pello & Luis Diaz
 * fabLAB Asturias
 * LABoral Centro de Arte y Creacion Industrial
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this code.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rgb.h"
#include <Arduino.h>

static RGB_Color rgb_colors[] = {{0,0,0}, {255,0,0}, {0,255,0}, {0,0,255}, {255,150,0}, {0, 255, 0}, {255,0,255}, {255,255,255}};   

// Set RGB color
void set_RGB(RGB_t* l)
{
    analogWrite(LED_R, 255-l->r);
    analogWrite(LED_G, 255-l->g);
    analogWrite(LED_B, 255-l->b);
}

// Set RGB color from color table
void set_RGB_value(uint8_t col)
{
    analogWrite(LED_R, 255-rgb_colors[col].rgb[0]);
    analogWrite(LED_G, 255-rgb_colors[col].rgb[1]);
    analogWrite(LED_B, 255-rgb_colors[col].rgb[2]);
}

// selects RGB color
void put_color(RGB_t* l, uint8_t r, uint8_t g, uint8_t b)
{
  l->r = r;
  l->g = g;
  l->b = b;
}

// selects RGB color from color table
void put_color_value(RGB_t* l, uint8_t col)
{ 
  // assign
  l->r = rgb_colors[col].rgb[0];
  l->g = rgb_colors[col].rgb[1];
  l->b = rgb_colors[col].rgb[2];
}
