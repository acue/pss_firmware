/*
 *
 * Affective Centered User Experiences
 * Portable Sensoring System (PSS) firmware
 * Copyright 2014 David Pello & Luis Diaz
 * fabLAB Asturias
 * LABoral Centro de Arte y Creacion Industrial
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this code.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <EEPROM.h>
#include <TimerOne.h>
#include <Wire.h>
#include <DS1337.h>
#include <CmdMessenger.h>
#include <I2Cdev.h>
#include <MPU6050.h>
//#include "MPU6050_6Axis_MotionApps20.h"
#include "pss.h"
#include "rgb.h"


// SENSORS UPDATE RATE
// use just one message for both GSR and HRM sensors OR use different
// messages for them, each with its own update rate
// 0 for one unique message, 1 for two independent messages.

// #define TWO_MESSAGES 


// GLOBALS
//////////////////////////////////////////////////////////////////////////
uint32_t pss_ID = 0;        		// PSS DEVICE ID
uint8_t logging = 0;      			// sending data
uint8_t alarm = 0;        			// alarm active
uint8_t alarm_type = LOW_BATT;  	// alarm type
uint8_t led_alarm_blink = 0;    	// alarm blinking 
uint8_t alarm_counter = 0;      	// alarm timer runs each 20ms. 
									// 		We don't need that much speed
RGB_t led_rgb = {0,0,0};			// to store current RGB LED color
RGB_t* led_rgb_p = &led_rgb; 		// pointer to current RGB LED color
uint16_t voltage;                	// Battery voltage (millivolts)
uint16_t hrm;                    	// Heart rate monitor data
uint16_t gsr;                    	// GSR data
float conductance;  				// conductance value calculated from GSR data
uint16_t conductance_millisiemens;	// *1000
uint16_t temperature;            	// temperature (1/10 ºC)
uint32_t main_timer=0;				// main timer at 20Hz
uint32_t milliseconds = 0;			// ms counter
uint16_t milliseconds_reset = 0;	// check milliseconds overflow            
uint16_t update_ms = 50;        	// update rate (20Hz)
uint16_t hrm_update_ms = 50;		// GSR update rate (20Hz)
uint16_t gsr_update_ms = 50; 		// HRM update rate (20Hz)
uint16_t ms_offset;            		// offset from millis()

char* last_error;        			// Status and Error Msgs

// Attach a new CmdMessenger object to the default Serial port
CmdMessenger cmdMessenger = CmdMessenger(Serial);

// Create the RTC Instance
DS1337 RTC = DS1337();




// CALLBACKS
//////////////////////////////////////////////////////////////////////////

// Callbacks define on which received commands we take action 
void attachCommandCallbacks()
{
    cmdMessenger.attach(kSetDateTime, onSetDateTime);
    cmdMessenger.attach(kSetLED, onSetLED);
    cmdMessenger.attach(kSessionStart, onSessionStart);
    cmdMessenger.attach(kSessionStop, onSessionStop);
    cmdMessenger.attach(kGetStatus, onGetStatus);
    cmdMessenger.attach(kSetUpdateRate, onSetUpdate);
    cmdMessenger.attach(kProgramID, onProgramID);
	cmdMessenger.attach(kSetGSRUpdateRate, onSetGSRUpdate);
	cmdMessenger.attach(kSetHRMUpdateRate, onSetHRMUpdate);
}

// Callback function to set Time on the RTC
void onSetDateTime()
{

    // read all the arguments, year, month, day, hour, minute, second
    RTC.setYears(cmdMessenger.readIntArg());
    RTC.setMonths(cmdMessenger.readIntArg());
    RTC.setDays(cmdMessenger.readIntArg());
    RTC.setHours(cmdMessenger.readIntArg());
    RTC.setMinutes(cmdMessenger.readIntArg());
    RTC.setSeconds(cmdMessenger.readIntArg());
    RTC.writeTime();
    milliseconds = 0; // last 3 digits
}

// Callback function to set RGB LED Color
void onSetLED()
{

    led_rgb_p->r=cmdMessenger.readIntArg();
    led_rgb_p->g=cmdMessenger.readIntArg();
    led_rgb_p->b=cmdMessenger.readIntArg();

    set_RGB(led_rgb_p);   
}

// Callback function to start logging
void onSessionStart()
{
    logging = 1;
    // logging, pink LED
    put_color_value(led_rgb_p, PINK);
    set_RGB(led_rgb_p);
}

// Callback function to start logging
void onSessionStop()
{
    logging = 0;
    // stopped logging, ready, green LED
    put_color_value(led_rgb_p, GREEN);
    set_RGB(led_rgb_p);
}


// Callback to send status
void onGetStatus()
{
    send_status_message(last_error);
}

// Timer callbacks
void alarm_callback()
{
    
    // increment millisecond count
    milliseconds += 1;
    if (milliseconds  > 999)
        milliseconds = 0;
    
    // return if < 300 ms from previous call
    if (milliseconds % 250 != 0)
        return;
    
    // no alarm, return
    if (!alarm)
        return;

    // low batt alarm
    if (alarm_type == LOW_BATT)
    {
        if (led_alarm_blink) // need to blink?
        {
            // change LED to low batt alarm
            set_RGB_value(RED);
            led_alarm_blink = 0;
        }
        else
        {
            // restore LED
            set_RGB(led_rgb_p);
            led_alarm_blink = 1;
        }
    }

    // calibrating
    if (alarm_type == CALIBRATING)
    {
        if (led_alarm_blink) // need to blink?
        {
            // change LED to low batt alarm
            set_RGB_value(BLACK);
            led_alarm_blink = 0;
        }
        else
        {
            // restore LED
            set_RGB(led_rgb_p);
            led_alarm_blink = 1;
        }
    }

}


// Set update rate
void onSetUpdate()
{
    uint16_t value = cmdMessenger.readIntArg();
    if (value >= 50 && value <=200)
        update_ms = value;
}

// Set GSR update rate
void onSetGSRUpdate()
{
    uint16_t value = cmdMessenger.readIntArg();
    if (value >= 50 && value <=200)
        gsr_update_ms = value;
}

// Set HRM update rate
void onSetHRMUpdate()
{
    uint16_t value = cmdMessenger.readIntArg();
    if (value >= 50 && value <=200)
        hrm_update_ms = value;
}


// Record ID on EEPROM
void onProgramID()
{
    uint32_t newID = cmdMessenger.readLongArg();
    uint8_t uint8_ts[4];
    
    uint8_ts[3] = (newID >> 24) & 0xFF;
    uint8_ts[2] = (newID >> 16) & 0xFF;
    uint8_ts[1] = (newID >> 8) & 0xFF;
    uint8_ts[0] = newID & 0xFF;
    
    EEPROM.write(0x00, uint8_ts[0]);
    EEPROM.write(0x01, uint8_ts[1]);
    EEPROM.write(0x02, uint8_ts[2]);
    EEPROM.write(0x03, uint8_ts[3]);
    
    pss_ID=newID;
}

// RTC interrupt
void rtc_interrupt_callback()
{
    // reset milliseconds
    milliseconds = 0;   
}

// MAIN FUNCTIONS
//////////////////////////////////////////////////////////////////////////


// Get data from Sensors
void update_sensors()
{
    main_timer=millis();
    hrm = analogRead(HRM_PIN);
    gsr = analogRead(GSR_PIN);
    float vgsr = (gsr * 5.0) / 1023.0;
    conductance = 2.0 * ((vgsr - GSR_VREF) / 100000.0);
    conductance = conductance * 1000000;
    if (conductance < 0)
        conductance = 0;
    RTC.readTime();
}

// Get voltage (millivolts)
uint16_t get_voltage()
{
    uint16_t v;

    v = analogRead(0);
    v = map(v, 0, V_CONV, 0, 4200);

    return v;
}


// Send message with all the sensor data
void send_sensors_message()
{
    cmdMessenger.sendCmdStart(kSensData);
    cmdMessenger.sendCmdArg(RTC.getYears());
    cmdMessenger.sendCmdArg(RTC.getMonths());
    cmdMessenger.sendCmdArg(RTC.getDays());
    cmdMessenger.sendCmdArg(RTC.getHours());
    cmdMessenger.sendCmdArg(RTC.getMinutes());
    cmdMessenger.sendCmdArg(RTC.getSeconds());
    cmdMessenger.sendCmdArg(milliseconds);
    cmdMessenger.sendCmdArg(hrm);
    cmdMessenger.sendCmdArg(conductance,4);
    cmdMessenger.sendCmdEnd();
}   

void send_status_message(char* msg)
{
    cmdMessenger.sendCmdStart(kPSSstat);
    cmdMessenger.sendCmdArg(pss_ID);
    cmdMessenger.sendCmdArg(get_voltage());
    cmdMessenger.sendCmdArg(msg);
    cmdMessenger.sendCmdEnd();  
}



// UTILITY FUNCTIONS
//////////////////////////////////////////////////////////////////////////
void eeprom_write_int(uint16_t p_address, uint16_t p_value)
{
    uint8_t lowByte = ((p_value >> 0) & 0xFF);
    uint8_t highByte = ((p_value >> 8) & 0xFF);

    EEPROM.write(p_address, lowByte);
    EEPROM.write(p_address + 1, highByte);
}

// This function will read a 2 uint8_t integer from the eeprom 
// at the specified address and address + 1
uint16_t eeprom_read_int(uint16_t p_address)
{
    uint8_t lowByte = EEPROM.read(p_address);
    uint8_t highByte = EEPROM.read(p_address + 1);

    return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}



void setup()
{
    // get ID from EEPROM
    // ID is located at addr 0x00, 0x01, 0x02, 0x03, LSB first
    pss_ID = EEPROM.read(0x00) + ((uint16_t)EEPROM.read(0x01)<<8) + 
		((uint32_t)EEPROM.read(0x02)<<16) + ((uint32_t)EEPROM.read(0x03)<<24);
    
    // allocate memory for error msg
    last_error = (char*) malloc(sizeof(char)*32);
    
    // Initialize the serial port at 115200 bauds
    Serial.begin(38400); 

    // RGB LED
    pinMode(LED_R, OUTPUT);
    pinMode(LED_G, OUTPUT);
    pinMode(LED_B, OUTPUT);
    
    // RTC interrupt pin
    pinMode(RTC_INT_PIN, INPUT);
    digitalWrite(RTC_INT_PIN, HIGH);

    // initializing, yellow light
    put_color_value(led_rgb_p, YELLOW);
    set_RGB(led_rgb_p);
    
    // ensure RTC oscillator is running, if not already
    RTC.start(); 
    delay(20);

    
    // RTC interrupt each second to reset millisecond count
    attachInterrupt(RTC_INT, rtc_interrupt_callback, FALLING);
    RTC.enable_interrupt();
    RTC.setAlarmRepeat(EVERY_SECOND);
    RTC.writeAlarm();    

    // Adds newline to every command
    cmdMessenger.printLfCr();
    // Attach my application's user-defined callback methods
    attachCommandCallbacks();

    // reset alarms
    alarm = 0;

    // Timer
    Timer1.initialize(995);
    Timer1.attachInterrupt(alarm_callback);

    // we are ok, green light
    put_color_value(led_rgb_p, GREEN);
    set_RGB(led_rgb_p);

    // small delay while the serial port gets ready
    delay(1000);
    // send status
    strcpy(last_error, "PSS OK");
    send_status_message(last_error);

}

void loop()
{

    // Process incoming serial data, and perform callbacks
    cmdMessenger.feedinSerialData(); 

    // Check voltage and set alarm
    voltage = get_voltage();
    if (voltage < 3400)
    {
        // set low voltage alarm
        alarm = 1;
        alarm_type = LOW_BATT;
        strcpy(last_error, "BATTERY LOW");
    }
    else
    {
        if (alarm == 1 && alarm_type == LOW_BATT)
       { // alarm was set but voltage is acceptable again
            alarm = 0;
            set_RGB(led_rgb_p);
            strcpy(last_error, "PSS OK");
       }
    }

#ifdef TWO_MESSAGES

#else

    // update loop 
    if (milliseconds%update_ms == 0 || milliseconds == 0) 
    {
        // we can't clear the interrupt on the callback
		// (breaks the program) so we test and call it here
        if (milliseconds==0)
        {
            RTC.clear_interrupt();    
        }
            
        
        // logging?
        if (logging)
        {
            update_sensors();
            send_sensors_message();
        }
        
    }
#endif

}







