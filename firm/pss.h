/*
 *
 * Affective Centered User Experiences
 * Portable Sensoring System (PSS) firmware
 * Copyright 2014 David Pello & Luis Diaz
 * fabLAB Asturias
 * LABoral Centro de Arte y Creacion Industrial
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this code.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __PSS_H
#define __PSS_H

// pins
#define HRM_PIN    1 // analog
#define GSR_PIN    2 // analog

#define RTC_INT         1 // INT1
#define RTC_INT_PIN     3 

// battery conversion macros
#define     VCC      3.3
#define     V_CONV   1023*2.1/VCC

// GSR Calibration
#define GSR_VREF     0.499

// Calibration macros
#define CALIBRATED       0xAA
#define CALIBRATED_ADDR  0x00
#define XMIN_ADDR        0x01
#define YMIN_ADDR        0x03
#define ZMIN_ADDR        0x05
#define XMAX_ADDR        0x07
#define YMAX_ADDR        0x09
#define ZMAX_ADDR        0x0B
#define CALIBRATION_TIME 20000


// IMU related macros
#define GRAVITY 256  //this equivalent to 1G in the raw data coming from the accelerometer 

#define ToRad(x) ((x)*0.01745329252)  // *pi/180
#define ToDeg(x) ((x)*57.2957795131)  // *180/pi

// L3G4200D gyro: 2000 dps full scale
// 70 mdps/digit; 1 dps = 0.07
#define Gyro_Gain_X 0.07 //X axis Gyro gain
#define Gyro_Gain_Y 0.07 //Y axis Gyro gain
#define Gyro_Gain_Z 0.07 //Z axis Gyro gain
#define Gyro_Scaled_X(x) ((x)*ToRad(Gyro_Gain_X)) //Return the scaled ADC raw data of the gyro in radians for second
#define Gyro_Scaled_Y(x) ((x)*ToRad(Gyro_Gain_Y)) //Return the scaled ADC raw data of the gyro in radians for second
#define Gyro_Scaled_Z(x) ((x)*ToRad(Gyro_Gain_Z)) //Return the scaled ADC raw data of the gyro in radians for second

#define Kp_ROLLPITCH 0.02
#define Ki_ROLLPITCH 0.00002
#define Kp_YAW 1.2
#define Ki_YAW 0.00002

/*For debugging purposes*/
//OUTPUTMODE=1 will print the corrected data, 
//OUTPUTMODE=0 will print uncorrected data of the gyros (with drift)
#define OUTPUTMODE 1

//#define PRINT_DCM 0     //Will print the whole direction cosine matrix
#define PRINT_ANALOGS 0 //Will print the analog raw data
#define PRINT_EULER 1   //Will print the Euler angles Roll, Pitch and Yaw

// ALARMS
#define  LOW_BATT  0
#define  CALIBRATING 1

#define ALARM_DELAY 15    // 20ms*15 = 300ms

// Commands
enum
{
  kSetDateTime, 		// set Time and Date
  kSetLED,      		// set RGB LED Color
  kNotUsed1, 			// start Compass calibration
  kNotUsed2,     		// start IMU calibration
  kSessionStart,    	// start logging session
  kSessionStop,    		// stop logging session
  kGetStatus,       	// ask for status
  kSensData,        	// data from the sensors
  kPSSstat,        		// status message
  kSetUpdateRate,  		// set update rate
  kProgramID,      		// program a new ID into EEPROM
  kGSRData,		  		// data from the GSR sensor
  kHRMData, 	  		// data from the HRM sensor
  kSetGSRUpdateRate, 	// set update rate for the GSR messages
  kSetHRMUpdateRate, 	// set update rate for the HRM messages
};

#endif


